const express = require('express')
const redis = require('redis'); // import d'un client redis
const app = express()

// variables d'env
const PORT = process.env.SERVER_PORT || 3000;
const REDIS_HOST = process.env.REDIS_HOST || 'localhost';
const REDIS_PORT = process.env.REDIS_PORT|| 6379;

// connection au serveur redis
const rediscli = redis.createClient({
  host: REDIS_HOST,
  port: REDIS_PORT
});

rediscli.set('visits', 0); // écriture en serveur redis

 
app.get('/', function (req, res) {
  res.send('Hello World')
})

app.get('/help', function (req, res) {
  res.send('Aiutoooooooooooooo !!!')
})

app.get('/redis', function (req, res) {
  
  // interrogation du serveur redis
  rediscli.get('visits', (err, visits) => {
    
    // envoie de la réponse au client http
    res.json({ visits });
    
    // incrémentation du nb de visites en base redis
    rediscli.set('visits', parseInt(visits) + 1);
  
  })

})
 

app.listen(PORT, () => {
  console.log('Server running on port ' + PORT + '...')
})
